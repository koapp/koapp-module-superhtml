(function() {
  'use strict';

  angular
    .module('superhtml', [])
    .controller('superhtmlController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location', '$rootScope', '$translate'];

  function loadFunction($scope, structureService, $location, $rootScope, $translate) {
    // Register upper level modules
    structureService.registerModule($location, $scope, 'superhtml', $translate.use());
    // --- Start superhtmlController content ---
    structureService.launchSpinner('.holds-the-iframe');
    $rootScope.isBusy  = true;
    var config = $scope.superhtml.modulescope;
    var configLang = $scope.superhtml.modulescopeLang;
    var lang = $translate.use().replace('_', '-');
    var iframeConfig = `
    <base target='_parent'></base>
    ${getLinks(structureService.getFonts(), "url")}
    
    <style>
        :root{
            ${getCssVars(structureService.getColors())}
            ${getCssVars(structureService.getFonts(), "name")}
        }
    </style>
    `;
    
 
    if (config.useTranslate) {
      $scope.content = iframeConfig + configLang[lang].codeLang
    } else {
      $scope.content = iframeConfig + config.code;
    }
    
    /*
    //code for demo uncoment this to test
    //get color vars
    console.log(getCssVars(structureService.getColors()));
    //get fonts vars
    console.log(getCssVars(structureService.getFonts(), "name" ));
    //get links for the fonts
    console.log(getLinks(structureService.getFonts(), "url"));
    */
    
    
    //tools 
    function getLinks(obj, urlVar){
        let links = "";
        
        Object.keys(obj).forEach(function(item){
            links += `<link href="${obj[item][urlVar]}" rel="stylesheet">`;
        });
        return links;
    }
    
    function getCssVars(obj, sub){
        let result = "";
        Object.keys(obj).forEach(function(item){
            let itemVal = sub ? `"${obj[item][sub]}"` : obj[item];
            result += `--${item.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase()}`;
            result += `: ${itemVal};`;
        });
        
        return result;
    }
        
    // --- End superhtmlController content ---
  }
}());
